/**
*@file
* This JS file controls all the JS functionalities
* of this module (like binding events to the gallery contollers, managing
* positions and visual effect settings).
*/

/**
* functions to do all JS magic.
*/
(function($) {
  $(document).ready(
    function() {
      var showcase = jQuery("#showcase");

      showcase.Cloud9Carousel({
        yPos: 42,
        yRadius: 48,
        mirrorOptions: {
          gap:12,
          height: 0.2
        },
        buttonLeft: jQuery(".nav > .left"),
        buttonRight: jQuery(".nav > .right"),
        autoPlay: true,
        bringToFront: true,
        onRendered: showcaseUpdated,
        onLoaded: function() {
          showcase.css("visibility", "visible")
          showcase.css("display", "none")
          showcase.fadeIn(1000)
        }
      });
      function showcaseUpdated(showcase) {
        var title = jQuery("#item-title").html(
          jQuery(showcase.nearestItem()).attr("alt")
          );
        var c = Math.cos((showcase.floatIndex() % 1) * 2 * Math.PI);
        title.css("opacity", 0.5 + (0.5 * c));
      };
      // Simulate physical button click effect
      jQuery(".nav > button").click(function(e) {
        var b = jQuery(e.target).addClass("down")
        setTimeout(function() {
          b.removeClass("down")
        }, 80)
      });

      jQuery(document).keydown(function(e) {
        switch(e.keyCode) {
          /* left arrow */
          case 37:
            jQuery(".nav > .left").click();break;

          /* right arrow */
          case 39:
            jQuery(".nav > .right").click()

        }
      });
    });
})(jQuery);
