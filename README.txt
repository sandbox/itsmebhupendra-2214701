Module
-------
Gallery 3D


CREDITS
--------

This module is Developed by Bhupendra Singh <itsmebhupendra at gmail dot com>


DESCRIPTION
-------------
This module provide a content type named "Gallery 3D" to create a album. Besides
content type it also provides a default "Albums" view to list all the Album
created and render every node in 3D Gallery.

DEPENDENCY
-------------
This module depends on the views and libraries API module.

INSTALLATION
-------------

# Download and copy module in modules folder.
# Download and Install dependent module in modules folder.
# Download the cloud9carousel library from
# https://github.com/specious/cloud9carousel
# and copy "jquery.cloud9carousel.js" file under
# "sites/all/libraries/gallery3d/" folder.
# Install the module.
# It will generate a content type Gallery 3D and a view Albums to list all the
albums.
# Nodes created by Gallery 3D content type will by default render in 3D
Gallery mode.

ABOUT CLOUD 9 CAROUSEL
-----------------------

Cleaned up, refactored, and improved version of [CloudCarousel]
(http://webscripts.softpedia.com/script/Image-Galleries/Image-Tools/
Cloud-Carousel-65275.html)
, a jQuery 3D image carousel originally released by [Professor Cloud](#authors).

## Features

- Just one JavaScript file
- Works with jQuery **or** Zepto
- Fast
- Easy to use
- *(optional)* Reflections (via [reflection.js]
(http://www.digitalia.be/software/reflectionjs-for-jquery))
- *(optional)* Mouse wheel support (via [mousewheel plugin]
(http://plugins.jquery.com/mousewheel/)) [see: [note](#known-issues)]
- *(optional)* Rotate clicked item to front
- *(optional)* Auto-play
- Create multiple instances
- Handy callback functions

## Dependencies

- [jQuery](https://github.com/jquery/jquery) 1.3.0 or later **or** [Zepto]
(https://github.com/madrobby/zepto) 1.1.1 or later
- Optional mirror effect using the [reflection.js plugin]
(http://www.digitalia.be/software/reflectionjs-for-jquery) by Christophe Beyls
(jQuery only)
- Optional mouse wheel support via the [mousewheel plugin]
(http://plugins.jquery.com/mousewheel/) (jQuery only)

## Authors

- Upgrades by [Ildar Sagdejev](http://twitter.com/tknomad)
- Forked from CloudCarousel v1.0.5 by [Professor Cloud]
(http://www.professorcloud.com/) (R. Cecco)

## Known issues

- Due to lack of standartisation, "mousewheel" scrolling is ridiculously
sensitive and unmanageable when using some track pads (such as on the MacBook
Pro).  Unfortunately, since there appears to be no way to know directly what
type of device is triggering the mousewheel events, it is not trivial to somehow
normalise or "tame" the input from the track pad without also affecting the
"1 tick per click" behaviour of the standard mouse wheel.  **darsain** has
described the same phenomenon in [this discussion]
(https://github.com/darsain/sly/issues/67) at the sly.js project.

## License

Licensed under the [MIT License](http://en.wikipedia.org/wiki/MIT_License)
