<?php
/**
 * @file
 * Render the node content.
 */
$images = $node->gallery3d_images[LANGUAGE_NONE];
?>

<div id="gallery3dwrap">
  <div id="showcase" class="noselect">

<?php
foreach ($images as $image) :
  print theme('image_style', array(
    'style_name' => 'Gallery3D',
    'path' => $image['uri'],
    'title' => $image['alt'],
    'alt' => $image['alt'],
    'getsize' => TRUE,
    'attributes' => array(
      'class' => 'cloud9-item',
    ),
  )
        );
endforeach;
?>

  </div>
  <p id="item-title">&nbsp;</p>
  <div class="nav noselect">
    <button class="left"> << </button>
    <button class="right"> >> </button>
  </div>
</div>
<div class="album-description">

<?php
if (!empty($node->body)) :
  echo $node->body[LANGUAGE_NONE][0]['value'];
endif;
?>

</div>
